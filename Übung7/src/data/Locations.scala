package data

import scala.collection._

//die Methode filter, die f�r �bung 7 erstellt werden musste, befindet sich im Objekt LocationListe 

case class Location(var name:String, val id:String, val erstelldatum:String, var besitzer:Benutzer,
    val geographischeDaten:(String,String)){
  //der Besitzer wird beim Erstellen automatisch gesetzt,
  //deshalb hier die 1-Beziehung
  
  var aenderungsdatum = erstelldatum
  
  var bewertung = -1  //Default-Wert
  var bemerkung:List[String] = Nil
  
  var kategorien:List[Kategorie] = Nil
  var adresse:Adresse = null
  var media:List[Media] = Nil
  
  var schreibrechte:List[Benutzer] = Nil
  var leserechte:List[Benutzer] = Nil
  
  def hinzufuegen = {
    assert(geographischeDaten._1.toDouble <= 180.0 && geographischeDaten._2.toDouble <= 360.0)
    assert(bewertung >= -1 && bewertung <= 5)
    
    LocationListe.locations += this
  }
  

}

case class Kategorie(var name:String){
  var icon:Foto = null
  var locations:List[Location] = Nil
}

case class Adresse(val strasse:String, val plz:Int,  val ort:String, val land:String){
  var telefon = ""
  var email = ""
  var website = "" 
}

abstract class Media(name:String){}

case class Foto(val name:String) extends Media(name){}
case class Video(val name:String) extends Media(name){}
case class Audio(val name:String) extends Media(name){}
//erben von Media, weil sie alle in die Media-Liste
//einer Location passen muessen

case class Gruppe(var name:String, var besitzer:Benutzer){
  var foto:Foto = null
  var locations:Map[Location, Boolean] = Map[Location, Boolean]()
  //wenn der Boolean-Wert true ist, haben Mitglieder
  //Schreiberechte, ist er false, haben sie nur
  //Leserechte
  var mitglieder:List[Benutzer] = besitzer :: Nil
  //so ist garantiert, dass die Liste schon 1 Element hat 
  //(mind. 1 Mitglied)
}

case class Benutzer(val name:String, val id:String){
  var freunde:List[Benutzer] = Nil
}

object LocationListe {
  val dummy = Benutzer("","")
  val defaultLoc = Location("","","",dummy,("0.0","0.0"))
  var locations:mutable.HashSet[Location] = mutable.HashSet[Location]()
  
    def loeschen(loc:Location) = {
    assert(locations.contains(loc))
    locations -= loc
  }
  
  def sortieren(key:String):immutable.TreeMap[String,mutable.HashSet[Location]] = {
    assert(key == "name" || key == "land" || key == "ort" || key == "bewertung")
    var locSort = immutable.TreeMap[String,mutable.HashSet[Location]]()
    if (key == "name") 
    //ist schon ein HashSet mit gegebenem Key in der TreeMap, so wird die Location in dieses eingefügt,
    // ansonsten wird ein neues mit diesem Key erstellt
      for(i <- locations) 
        if(locSort.contains(i.name)) locSort(i.name) += i 
        else locSort = locSort.+((i.name,mutable.HashSet[Location](i)))
    else if (key == "land")
      for(i <- locations) {
        if(i.adresse != null)
          if(locSort.contains(i.adresse.land)) locSort(i.adresse.land) += i
          else locSort = locSort.+((i.adresse.land,mutable.HashSet[Location](i)))
      }
    else if (key == "ort")
      for(i <- locations) {
        if(i.adresse != null)
          if(locSort.contains(i.adresse.ort)) locSort(i.adresse.ort) += i
          else locSort = locSort.+((i.adresse.ort,mutable.HashSet[Location](i)))
      }
    else if (key == "bewertung")
      for(i <- locations) 
        if (i.bewertung == -1)
          if(locSort.contains("6")) locSort("6") += i 
          else locSort = locSort.+(("6",mutable.HashSet[Location](i)))
        else
          if(locSort.contains(i.bewertung.toString)) locSort(i.bewertung.toString) += i 
          else locSort = locSort.+((i.bewertung.toString,mutable.HashSet[Location](i)))
    
    locSort
  }
  
  def filter(kriterien:Map[String,String]):mutable.HashSet[Location] = {
    val gueltig = Array("Bewertung","Kategorie","LetzteAenderung","Erstelldatum","Ort","Land","Besitzer","Name","Entfernung")
    assert(!kriterien.isEmpty)
    assert(kriterien.keys.forall {x => gueltig.contains(x)})
    var locs = LocationListe.locations
    for(i <- kriterien) i._1 match {
      case "Bewertung" => for(j <- locs) if(j.bewertung < i._2.toInt) locs -= j
      case "Kategorie" => for(j <- locs) {
                            var katSuche = false
                            for(k <- j.kategorien) if(k.name == i._2) katSuche = true 
                            if(!katSuche) locs -= j
                          }
      case "LetzteAenderung" => for(j <- locs) if(!zeitraum(i._2, j.aenderungsdatum)) locs -= j
      case "Erstelldatum" => for(j <- locs) if(!zeitraum(i._2, j.erstelldatum)) locs -= j
      case "Ort" => for(j <- locs) if(j.adresse == null || j.adresse.ort != i._2) locs -= j
      case "Land" => for(j <- locs) if(j.adresse == null || j.adresse.land != i._2) locs -= j
      case "Besitzer" => for(j <- locs) if(j.besitzer.name != i._2) locs -=j
      case "Name" => for(j <- locs) if(!j.name.contains(i._2)) locs -= j
      case "Entfernung" => for(j <- locs) if(entfernung(j.geographischeDaten) > i._2.toDouble) locs -= j
    }
    locs
  }
  
  def zeitraum(z:String, d:String):Boolean = {
    val zSplit = z.split('-')
    val zAnfang = zSplit(0).split('.')
    val zEnde = zSplit(1).split('.')
    val dSplit = d.split('.')
    if(dSplit(2).toInt < zAnfang(2).toInt || dSplit(2).toInt > zEnde(2).toInt) false
    else if((dSplit(2).toInt == zAnfang(2).toInt && dSplit(1).toInt < zAnfang(1).toInt) 
         || (dSplit(2).toInt == zEnde(2).toInt   && dSplit(1).toInt > zEnde(1).toInt)) false
    else if((dSplit(2).toInt == zAnfang(2).toInt && dSplit(1).toInt == zAnfang(1).toInt && dSplit(0).toInt < zAnfang(0).toInt)
         || (dSplit(2).toInt == zEnde(2).toInt   && dSplit(1).toInt == zEnde(1).toInt && dSplit(0).toInt > zEnde(0).toInt)) false
    else true     
  }
  
  val deinePosition = ((42.0,42.0))
  def entfernung(koords:(String,String)):Double = {
    val lat1 = koords._1.toDouble
    val lon1 = koords._2.toDouble
    
    val lat2 = deinePosition._1
    val lon2 = deinePosition._2
    
    val lat = (lat1 + lat2) / 2 * 0.01745
    val dx = 111.3 * math.cos(lat) * (lon1 - lon2)
    val dy = 111.3 * (lat1 - lat2)
    
    math.sqrt(dx * dx + dy * dy)
  }
}