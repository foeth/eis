import data.LocationListe

import data.Location

import data.Kategorie

import data.Benutzer

import data.Adresse

object Main {
  def main(args:Array[String]) {

    //hier Test zu Uebung 7; wir haben gleich alle Faelle getestet, weil (ja, weil)
    
    val batman = Benutzer("Batman","00000007")
    val superman = Benutzer("Superman","00000001")
    
    val kneipe = Kategorie("Kneipe")
   
    val batAdresse = Adresse("Batstrasse 1", 3, "Gotham City", "USA")

    val batcave = Location("Batcave", "00000001", "07.12.2015", batman, ("0.0","0.0"))
    batcave.adresse = batAdresse
   
    batcave.hinzufuegen

    println("Die Location-Liste:")
    for(i <- LocationListe.locations)  println(i)
   
    val gothamS = Map[String,String]("Ort" -> "Gotham City")
    val usaS = Map[String,String]("Land" -> "USA")
    val supermanS = Map[String,String]("Besitzer" -> "Superman")
    val kneipeS = Map[String,String]("Kategorie" -> "Kneipe") 
    
    println("\nGesuchtes mit Stadt Gotham City:")
    for(i <- LocationListe.filter(gothamS)) println(i)
    println("\nGesuchtes mit Land USA:")
    for(i <- LocationListe.filter(usaS)) println(i)
    println("\nGesuchtes mit Besitzer Superman:")
    for(i <- LocationListe.filter(kneipeS)) println(i)
    println("\nGesuchtes mit Katgorie Kneipe:")
    for(i <- LocationListe.filter(kneipeS)) println(i)
  
  }
}