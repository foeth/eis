package test

import scala.collection._
import org.junit.Test
import org.junit.Assert._
import data._
import scala.reflect.internal.util.HashSet

class testFiltern {
  val benutzerbatman = Benutzer("Batman","00000007")
  val benutzerkaesemann = Benutzer("Kaesemann","00000001")
    
  val geheim = Kategorie("Geheimversteck")
   
  val batAdresse = Adresse("Batstrasse 1", 3, "Gotham City", "USA")
  
  val locationkaesehaus = Location("Kaesehaus", "00000002", "01.01.2014", benutzerkaesemann, ("0.0","0.0"))
  val locationbatcave = Location("Batcave", "00000001", "07.12.2015", benutzerbatman, ("41.0","42.0"))
    locationbatcave.adresse = batAdresse
    locationbatcave.bewertung = 4
    locationbatcave.kategorien ::= geheim
  
  locationkaesehaus.hinzufuegen  
  locationbatcave.hinzufuegen
  var batSet:mutable.HashSet[Location] = mutable.HashSet[Location](locationbatcave)
  val bewMap = Map[String,String]("Bewertung" -> "4")
  val bewMapF = Map[String,String]("Bewertung" -> "5")
  val katMap = Map[String,String]("Kategorie" -> "Geheimversteck")
  val katMapF = Map[String,String]("Kategorie" -> "D�nerbude")
  val aendMap = Map[String,String]("LetzteAenderung" -> "07.12.2015-08.12.2015")
  val aendMapF = Map[String,String]("LetzteAenderung" -> "05.12.2015-06.12.2015")
  val erstMap = Map[String,String]("Erstelldatum" -> "07.12.2015-08.12.2015")
  val erstMapF = Map[String,String]("Erstelldatum" -> "05.12.2015-06.12.2015")
  val ortMap = Map[String,String]("Ort" -> "Gotham City")
  val ortMapF = Map[String,String]("Ort" -> "Metropolis")
  val landMap = Map[String,String]("Land" -> "USA")
  val landMapF = Map[String,String]("Land" -> "Deutschland")
  val besMap = Map[String,String]("Besitzer" -> "Batman")
  val besMapF = Map[String,String]("Besitzer" -> "Superman")
  val nameMap = Map[String,String]("Name" -> "cave")
  val nameMapF = Map[String,String]("Name" -> "cheese")
  val entfMap = Map[String,String]("Entfernung" -> "200")
  val entfMapF = Map[String,String]("Entfernung" -> "0")
  val leerMap = Map[String,String]()
  val falschMap = Map[String,String]("Schnitzel" -> "Wiener")
  
  @Test def leereLocationListeFilter = {
    LocationListe.loeschen(locationbatcave)
    LocationListe.loeschen(locationkaesehaus)
    assertTrue(LocationListe.filter(besMap).isEmpty)
  }
  
  @Test def bewertungFilter = {
    assertEquals(batSet , LocationListe.filter(bewMap))
    assertTrue(LocationListe.filter(bewMapF).isEmpty)
  }
  
  @Test def kategorieFilter = {
    assertEquals(batSet , LocationListe.filter(katMap))
    assertTrue(LocationListe.filter(katMapF).isEmpty)
  }
  
  
  @Test def letzeAenderungFilter = {
    assertEquals(batSet, LocationListe.filter(aendMap))
    assertTrue(LocationListe.filter(aendMapF).isEmpty)
  }
  
  @Test def erstelldatumFilter = {
    assertEquals(batSet, LocationListe.filter(erstMap))
    assertTrue(LocationListe.filter(erstMapF).isEmpty)
  }
  
  @Test def ortFilter = {
    assertEquals(batSet, LocationListe.filter(ortMap))
    assertTrue(LocationListe.filter(ortMapF).isEmpty)
  }
  
  @Test def landFilter = {
    assertEquals(batSet, LocationListe.filter(landMap))
    assertTrue(LocationListe.filter(landMapF).isEmpty)
  }
  
  @Test def besitzerFilter = {
    assertEquals(batSet, LocationListe.filter(besMap))
    assertTrue(LocationListe.filter(besMapF).isEmpty)
  }
  
  @Test def nameFilter = {
    assertEquals(batSet, LocationListe.filter(nameMap))
    assertTrue(LocationListe.filter(nameMapF).isEmpty)
  }
  
  @Test def entfernFilter = {
    assertEquals(batSet, LocationListe.filter(entfMap))
    assertTrue(LocationListe.filter(entfMapF).isEmpty)
  }
  
  @Test def leererFilter = {
    var leerMapPruef = false
    try {LocationListe.filter(leerMap)} catch {case e:java.lang.AssertionError => leerMapPruef = true}
    assertTrue(leerMapPruef)
  }
  
  @Test def falscherFilter = {
    var falschFiltPruef = false
    try {LocationListe.filter(falschMap)} catch {case e:java.lang.AssertionError => falschFiltPruef = true}
    assertTrue(falschFiltPruef)
  }
  
  @Test def besitzerUndLandFilter = {
    assertEquals(batSet, LocationListe.filter(besMap ++ landMap))
    assertTrue(LocationListe.filter(besMap ++ landMapF).isEmpty)
  }
}

/*c.
Diese Tests realisieren eine komplette Code�berdeckung, da es zu jedem Filterkriterium,
sowie zu einer Kombination von Filterkriterien einen postiven und einen negativen Test
gibt. Au�erdem wird auch auf das Filtern in einer leeren LocationListe und auf das
Eintretet der Assertion der Methode "filter" getestet.
*/
