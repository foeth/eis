class Location(var name:String, val id:String,
    val erstelldatum:String, var besitzer:Benutzer){
  val geographischeDaten:Array[Int] = new Array[Int](6)
  //der Besitzer wird beim Erstellen automatisch gesetzt,
  //deshalb hier die 1-Beziehung
  
  var aenderungsdatum = erstelldatum
  
  var bewertung = -1  //Default-Wert
  var bemerkung:List[String] = Nil
  
  var kategorien:List[Kategorie] = Nil
  var adresse:Adresse = null
  var media:List[Media] = Nil
  
  var schreibrechte:List[Benutzer] = Nil
  var leserechte:List[Benutzer] = Nil
}

class Kategorie(var name:String){
  var icon:Foto = null
  var locations:List[Location] = Nil
}

class Adresse(val strasse:String,val hausNr:String,
    val ort:String, val land:String, location:Location){
  //eine Adresse braucht eine zugegörige Location, weil sie
  //sonst unnoetig waere
  var telefon = ""
  var email = ""
  var website = "" 
}

abstract class Media(name:String){}

class Foto(val name:String) extends Media(name){}
class Video(val name:String) extends Media(name){}
class Audio(val name:String) extends Media(name){}
//erben von Media, weil sie alle in die Media-Liste
//einer Location passen muessen

class Gruppe(var name:String, var besitzer:Benutzer){
  var foto:Foto = null
  var locations:Map[Location, Boolean] = Map[Location, Boolean]()
  //wenn der Boolean-Wert true ist, haben Mitglieder
  //Schreiberechte, ist er false, haben sie nur
  //Leserechte
  var mitglieder:List[Benutzer] = besitzer :: Nil
  //so ist garantiert, dass die Liste schon 1 Element hat 
  //(mind. 1 Mitglied)
}

class Benutzer(val name:String, val ID:String){
  var freunde:List[Benutzer] = Nil
}

//b
//Der Code zum Entwurfsmodell passt, weil wir jede Klasse 
//aus dem Entwurfsmodell mit allen Feldern implementiert und
//jede Beziehung mit einer Kardinalitaet: von 1 oder (1...*) 
//umgesetzt haben. Eine erneute Ueberarbeitung des 
//Entwurfsmodells war nicht von Noeten, da es zu keinen 
//Komplikationen bei der Uebersetzung des Modells in 
//Scala-Code kam.

//c als Kommentare am Code