import scala.collection._
import java.util.Calendar

case class Location(var name:String, val id:String,
    val erstelldatum:String, var besitzer:Benutzer,
    val geographischeDaten:Array[Int]){
  //der Besitzer wird beim Erstellen automatisch gesetzt,
  //deshalb hier die 1-Beziehung
  
  var aenderungsdatum = erstelldatum
  
  var bewertung = -1  //Default-Wert
  var bemerkung:List[String] = Nil
  
  var kategorien:List[Kategorie] = Nil
  var adresse:Adresse = null
  var media:List[Media] = Nil
  
  var schreibrechte:List[Benutzer] = Nil
  var leserechte:List[Benutzer] = Nil
  
  def hinzufuegen = {
    val geo = geographischeDaten
    assert(geo.length == 6)
    
    val sekMinBreite = Array(1,2)
    val sekMinLaenge = Array(4,5)
    
    var geoPruefer = true
    if(geo(0)>360 || geo(3) > 180) geoPruefer = false
    for(i <- sekMinBreite)
      if(geo(i) != 360) {
        if (geo(i) >= 60) geoPruefer = false
      }
      else if (geo(i) != 0) geoPruefer = false
    for(i <- sekMinLaenge)
      if(geo(i) != 180) {
        if (geo(i) >= 60) geoPruefer = false
      }
      else if (geo(i) != 0) geoPruefer = false
    assert(geoPruefer) //Stellt mithilfe vorangegangener Schleifen sicher, dass die Koordinaten die richtige Form haben
    assert(bewertung >= -1 && bewertung <= 5)
    
    LocationListe.locations += this
  }
}

case class Kategorie(var name:String){
  var icon:Foto = null
  var locations:List[Location] = Nil
}

case class Adresse(val strasse:String,val hausNr:String,
    val ort:String, val land:String){
  var telefon = ""
  var email = ""
  var website = "" 
}

abstract class Media(name:String){}

case class Foto(val name:String) extends Media(name){}
case class Video(val name:String) extends Media(name){}
case class Audio(val name:String) extends Media(name){}
//erben von Media, weil sie alle in die Media-Liste
//einer Location passen muessen

case class Gruppe(var name:String, var besitzer:Benutzer){
  var foto:Foto = null
  var locations:Map[Location, Boolean] = Map[Location, Boolean]()
  //wenn der Boolean-Wert true ist, haben Mitglieder
  //Schreiberechte, ist er false, haben sie nur
  //Leserechte
  var mitglieder:List[Benutzer] = besitzer :: Nil
  //so ist garantiert, dass die Liste schon 1 Element hat 
  //(mind. 1 Mitglied)
}

case class Benutzer(val name:String, val id:String){
  var freunde:List[Benutzer] = Nil
}

object LocationListe {
  var locations:mutable.HashSet[Location] = mutable.HashSet[Location]()
  
  def loeschen(location:Location) = {
    assert(locations.contains(location))
    locations -= location
  }
  
  def sortieren(key:String):immutable.TreeMap[String,mutable.HashSet[Location]] = {
    assert(key == "name" || key == "land" || key == "ort" || key == "bewertung")
    var locSort = immutable.TreeMap[String,mutable.HashSet[Location]]()
    if (key == "name") 
    //ist schon ein HashSet mit gegebenem Key in der TreeMap, so wird die Location in dieses eingefügt,
    // ansonsten wird ein neues mit diesem Key erstellt
      for(i <- locations) 
        if(locSort.contains(i.name)) locSort(i.name) += i 
        else locSort = locSort.+((i.name,mutable.HashSet[Location](i)))
    else if (key == "land")
      for(i <- locations) {
        if(i.adresse != null)
          if(locSort.contains(i.adresse.land)) locSort(i.adresse.land) += i
          else locSort = locSort.+((i.adresse.land,mutable.HashSet[Location](i)))
      }
    else if (key == "ort")
      for(i <- locations) {
        if(i.adresse != null)
          if(locSort.contains(i.adresse.ort)) locSort(i.adresse.ort) += i
          else locSort = locSort.+((i.adresse.ort,mutable.HashSet[Location](i)))
      }
    else if (key == "bewertung")
      for(i <- locations) 
        if (i.bewertung == -1)
          if(locSort.contains("6")) locSort("6") += i 
          else locSort = locSort.+(("6",mutable.HashSet[Location](i)))
        else
          if(locSort.contains(i.bewertung.toString)) locSort(i.bewertung.toString) += i 
          else locSort = locSort.+((i.bewertung.toString,mutable.HashSet[Location](i)))
    
    locSort
  }
  
  def suche(krit:Map[String,String]):immutable.TreeMap[String,mutable.HashSet[Location]] = {
    assert(!krit.isEmpty)
    val gueltig = Array("land","ort","kategorie")
    assert(krit.keys.forall {x => gueltig.contains(x)})
    
    var ergSet = mutable.HashSet[Location]()
    
    if(krit.head._1 == "land") {
      ergSet = sortieren("land")(krit.head._2)
    }
    else if(krit.head._1 == "ort") {
      ergSet = sortieren("ort")(krit.head._2)
    }
    else ergSet = locations
    
    //mit dem head wird, wenn er nicht Kategorie als Key hat, eine HashSet erzeugt, da dies in der TreeMap vom Suchen einfach zu finden ist
    
    if(!krit.tail.isEmpty || krit.head._1 == "kategorie")  //ist die Liste langer als 1 (oder der erste Key ist Kategorie), so wird das bereis gefilterte Set nach weiteren Kriterien gefiltert  
    //das mit der Kategorie funktioniert nicht, aber es ist mir zu spaet, um noch darueber nachzudenken
      for(i <- krit.tail)
        if(i._1 == "land")
          for(j <- ergSet) {
            if(j.adresse.land !=  i._2) ergSet -= j
          }
        else if(i._1 == "ort") 
          for(j <- ergSet) {
            if(j.adresse.ort !=  i._2) ergSet -= j
          }
        else 
          for(j <- ergSet) {
            var katSuche = false
            for(k <- j.kategorien)
              if(k.name == i._2) katSuche = true
            if(!katSuche) ergSet -= j
          }
    
    var erg = immutable.TreeMap[String,mutable.HashSet[Location]]()
    for(i <- ergSet) 
        if(erg.contains(i.name)) erg(i.name) += i 
        else erg = erg.+((i.name,mutable.HashSet[Location](i)))
    erg
    //am Ende entseht wieder eine TreeMap, die nach namen sortiert ist
  }
}


object Main {
  def main(args:Array[String]) {
    val datum = Calendar.getInstance.getTime.toString
    
    val batman = Benutzer("Batman","00000007")
    val superman = Benutzer("Superman", "12345689")
    val manhunter = Benutzer("Martian Manhunter", "92375224")
    val statham = Benutzer("Jason Statham", "00000000")
    
    val geheim = Kategorie("Geheimversteck")
    val kalt = Kategorie("Kalt")
    
    val kaeseAdresse = Adresse("Kaesestrasse", "2", "Hungerhausen", "USA")
    val batAdresse = Adresse("Batstrasse", "1", "Gotham City", "USA")
    val krankAdresse = Adresse("Superkrebsweg", "1a", "C-Town", "Kanada")
    
    val kaesehaus = Location("Kaesehaus", "00000004", datum, batman, Array(1,2,3,4,5,6))
    kaesehaus.bewertung = 1
    kaesehaus.adresse = kaeseAdresse
    val batcave = Location("Batcave", "00000001", datum, batman, Array(1,2,3,4,5,6))
    batcave.bewertung = 5
    batcave.adresse = batAdresse
    batcave.kategorien ::= geheim
    val einsam = Location("Festung der Einsamkeit", "00000002", datum, superman, Array(1,2,3,4,5,6))
    einsam.bewertung = 5
    einsam.kategorien :::= List[Kategorie](geheim,kalt)
    val wachturm = Location("Wachturm", "00000003", datum, manhunter, Array(1,2,3,4,5,6))
    wachturm.bewertung = 3
    val krank = Location("Krankenhaus", "00000005", datum, statham, Array(1,2,3,4,5,6))
    krank.adresse = krankAdresse
    
    batcave.hinzufuegen
    einsam.hinzufuegen
    wachturm.hinzufuegen
    kaesehaus.hinzufuegen
    krank.hinzufuegen
    
    println("Die Location-Liste:")
    for(i <- LocationListe.locations)  println(i)
   
    println("\nSortiert nach Namen:")
    val sort = LocationListe.sortieren("name")
    for(j <- sort) {
      println("Name = " + j._1 + ":")
      for(j2 <- j._2) println(j2)
    }
   
    println("\nSortiert nach Bewertung:")
    val sort2 = LocationListe.sortieren("bewertung")
    for(k <- sort2) {
      println("Bewertung = " + k._1 + ":")
      for(k2 <- k._2) println(k2)
    }
    
    println("\nSortiert nach Ort:")
    val sort3 = LocationListe.sortieren("ort")
    for(k <- sort3) {
      println("Ort = " + k._1 + ":")
      for(k2 <- k._2) println(k2)
    }
    
    println("\nSortiert nach Land:")
    val sort4 = LocationListe.sortieren("land")
    for(k <- sort4) {
      println("Land = " + k._1 + ":")
      for(k2 <- k._2) println(k2)
    }
    

    
    val m = Map[String,String]("land" -> "USA", "ort" -> "Gotham City")
    val n = Map[String,String]("kategorie" -> "Geheimversteck")
    
    println("\nGesuchtes mit m:")
    for(i <- LocationListe.suche(m)) println(i)
    println("\nGesuchtes mit n:")
    for(i <- LocationListe.suche(n)) println(i)
    
    LocationListe.loeschen(batcave)
    println("\nDie Location-Liste nach loeschen:")
    for(i <- LocationListe.locations)  println(i)

  }
}



//b
//Der Code zum Entwurfsmodell passt, weil wir jede Klasse 
//aus dem Entwurfsmodell mit allen Feldern implementiert und
//jede Beziehung mit einer Kardinalitaet: von 1 oder (1...*) 
//umgesetzt haben. Eine erneute Ueberarbeitung des 
//Entwurfsmodells war nicht von Noeten, da es zu keinen 
//Komplikationen bei der Uebersetzung des Modells in 
//Scala-Code kam.

//c als Kommentare am Code