import org.json.simple.JSONArray
import org.json.simple.JSONObject
import java.util.HashMap
import java.util.ArrayList
import java.io.PrintWriter

object ExporterJSON {
  def export(locations:List[Location],categories:List[Kategorie],dateiname:String) {
    var ausgabe = new JSONObject().asInstanceOf[HashMap[Any,Any]]
    var features = new JSONArray
    var cate = new JSONArray
    
    for(location <- locations){
      var jsonLoc = new JSONObject().asInstanceOf[HashMap[Any,Any]]
      exportLocs(location,jsonLoc)
      features.asInstanceOf[ArrayList[JSONObject]].add(jsonLoc.asInstanceOf[JSONObject])
    }
    ausgabe.put("features", features)
    
    
    for(category <- categories){
      var ausgabeLoc = new JSONObject().asInstanceOf[HashMap[Any,Any]]
      exportCats(category,ausgabeLoc)
      cate.asInstanceOf[ArrayList[JSONObject]].add(ausgabeLoc.asInstanceOf[JSONObject])
    }
    ausgabe.put("categories", cate)
    
    new PrintWriter(dateiname){
      write(ausgabe.asInstanceOf[JSONObject].toJSONString())
      close
    }
    
  }
  
  def exportLocs(location:Location,ausgabeLoc:HashMap[Any,Any]) {
    var prop = new JSONObject().asInstanceOf[HashMap[Any,Any]]   
    
    prop.put("name",location.name)
    prop.put("note", location.bemerkung.toString)
    prop.put("address", location.adresse.strasse)
    prop.put("zipcode", location.adresse.plz)
    prop.put("village", location.adresse.ort)    
    prop.put("web", location.adresse.website)
    prop.put("rating", location.bewertung)
    prop.put("phone", location.adresse.telefon)
    prop.put("email", location.adresse.email)
    prop.put("creationDate", location.erstelldatum)
    prop.put("editDate", location.aenderungsdatum)
    prop.put("owner", location.besitzer.name)
    prop.put("type","Feature")

    var kat = new JSONArray
    for(i <- location.kategorien){
      kat.asInstanceOf[ArrayList[String]].add(i.name)
    }
    prop.put("categories", kat)     
    
    ausgabeLoc.put("properties",prop)
    
    var geom = new JSONObject().asInstanceOf[HashMap[Any,Any]]
    var koo = new JSONArray
    koo.asInstanceOf[ArrayList[String]].add(location.geographischeDaten._1)
    koo.asInstanceOf[ArrayList[String]].add(location.geographischeDaten._2)

    prop.put("coordinates",koo)    
  }  
  
  def exportCats(kat:Kategorie,ausgabeLoc:HashMap[Any,Any]) {
    ausgabeLoc.put("name",kat.name)  
    ausgabeLoc.put("icon",kat.icon.name)
    ausgabeLoc.put("type","Category")
  }
}