import java.io.FileReader
import java.util.Scanner
import java.io.IOException

object Importer {
  def liesLoc(dateiname:String):List[Location] = {
    var locations:List[Location] = Nil
    var fl = true 
    
    try {
      val fr = new FileReader(dateiname)
      val scanner = new Scanner(fr).useDelimiter("(;|\\n)")
      while (scanner.hasNext) {
        if (fl) {
          fl = false
          scanner.nextLine
        }
        else {
          locations ::= liesLocationEin(scanner)
          if(scanner.hasNext)scanner.nextLine
        }
      }
    } catch {
      case e: IOException => println(e.getMessage)
    }
    locations
  }
  
  def liesLocationEin(s:Scanner):Location = {
    var id = ""
    var name = ""
    var note:List[String] = Nil
    var bewertung = -1
    var crDate = ""
    var edDate = ""
    var brt = ""
    var lng = ""
    
    var strasse = ""
    var plz = 0
    var ort = ""
    var land = ""
    var web = ""
    var telefon = ""
    var email = ""

    var besitzer = ""
    
    var kategorien:Array[String] = new Array(0)
    
    if(s.hasNext) s.next
    if(s.hasNextInt) id = s.nextInt.toString
    if(s.hasNext) name = s.next
    if(s.hasNext) note ::= s.next
    if(s.hasNext) strasse = s.next
    if(s.hasNextInt) plz = s.nextInt
    if(s.hasNext) ort = s.next
    if(s.hasNext) land = s.next
    if(s.hasNext) web = s.next
    if(s.hasNextInt) note ::= "Gruendungsjahr: " + s.next.toString
    if(s.hasNextInt) bewertung = s.nextInt
    if(s.hasNext) telefon = s.next
    if(s.hasNext) email = s.next
    if(s.hasNext) crDate = s.next
    if(s.hasNext) edDate = s.next
    if(s.hasNext) besitzer = s.next
    if(s.hasNext) kategorien = s.next.split(",")
    if(s.hasNext) s.next
    if(s.hasNext) brt = s.next
    if(s.hasNext) lng = s.next
    
    val benutzer = Benutzer(besitzer, (math.random*10000).toInt.toString)
    val location = Location(name, id, crDate, benutzer, (brt,lng))
    val adresse = Adresse(strasse, plz, ort, land)
    
    adresse.website = web
    adresse.email = email
    adresse.telefon = telefon
    
    location.adresse = adresse
    location.bemerkung = note
    location.bewertung = bewertung
    location.aenderungsdatum = edDate
    for(i <- kategorien) location.kategorien ::= Kategorie(i)
    
    location
  }
  
  def liesKat(dateiname:String):List[Kategorie] = {
    var kategorien:List[Kategorie] = Nil
    var fl = true 
    
    try {
      val fr = new FileReader(dateiname)
      val scanner = new Scanner(fr).useDelimiter("(;|\\n)")
      while (scanner.hasNext) {
        if (fl) {
          fl = false
          scanner.nextLine
        }
        else {
          kategorien ::= liesKategorienEin(scanner)
          if(scanner.hasNext)scanner.nextLine
        }
      }
    } catch {
      case e: IOException => println(e.getMessage)
    }
    kategorien
  }
  
  def liesKategorienEin(s:Scanner):Kategorie = {
    var name = ""
    var id = ""
    
    var iconName = ""
    
    if(s.hasNext) s.next
    if(s.hasNext) id = s.next
    if(s.hasNext) name = s.next
    if(s.hasNext) iconName = s.next
    
    val kategorie = Kategorie(name)
    val icon = Foto(iconName)
    kategorie.icon = icon
    
    kategorie
  }
  

}


