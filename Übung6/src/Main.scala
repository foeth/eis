import java.util.Calendar
object Main {
  def main(args:Array[String]) {

    //hier Test zu Uebung 5
    val datum = Calendar.getInstance.getTime.toString
    
    val batman = Benutzer("Batman","00000007")
    val superman = Benutzer("Superman", "12345689")
    val manhunter = Benutzer("Martian Manhunter", "92375224")
    val statham = Benutzer("Jason Statham", "00000000")
    
    val geheim = Kategorie("Geheimversteck")
    val kalt = Kategorie("Kalt")
    
    val kaeseAdresse = Adresse("Kaesestrasse 2", 2, "Hungerhausen", "USA")
    val batAdresse = Adresse("Batstrasse 1", 3, "Gotham City", "USA")
    val krankAdresse = Adresse("Superkrebsweg 1a", 4, "C-Town", "Kanada")
    
    val kaesehaus = Location("Kaesehaus", "00000004", datum, batman, ("0.0","0.0"))
    kaesehaus.bewertung = 1
    kaesehaus.adresse = kaeseAdresse
    val batcave = Location("Batcave", "00000001", datum, batman, ("0.0","0.0"))
    batcave.bewertung = 5
    batcave.adresse = batAdresse
    batcave.kategorien ::= geheim
    val einsam = Location("Festung der Einsamkeit", "00000002", datum, superman, ("0.0","0.0"))
    einsam.bewertung = 5
    einsam.kategorien :::= List[Kategorie](geheim,kalt)
    val wachturm = Location("Wachturm", "00000003", datum, manhunter, ("0.0","0.0"))
    wachturm.bewertung = 3
    val krank = Location("Krankenhaus", "00000005", datum, statham, ("0.0","0.0"))
    krank.adresse = krankAdresse
    
    batcave.hinzufuegen
    einsam.hinzufuegen
    wachturm.hinzufuegen
    kaesehaus.hinzufuegen
    krank.hinzufuegen
    
    println("Die Location-Liste:")
    for(i <- LocationListe.locations)  println(i)
   
    println("\nSortiert nach Namen:")
    val sort = LocationListe.sortieren("name")
    for(j <- sort) {
      println("Name = " + j._1 + ":")
      for(j2 <- j._2) println(j2)
    }
   
    println("\nSortiert nach Bewertung:")
    val sort2 = LocationListe.sortieren("bewertung")
    for(k <- sort2) {
      println("Bewertung = " + k._1 + ":")
      for(k2 <- k._2) println(k2)
    }
    
    println("\nSortiert nach Ort:")
    val sort3 = LocationListe.sortieren("ort")
    for(k <- sort3) {
      println("Ort = " + k._1 + ":")
      for(k2 <- k._2) println(k2)
    }
    
    println("\nSortiert nach Land:")
    val sort4 = LocationListe.sortieren("land")
    for(k <- sort4) {
      println("Land = " + k._1 + ":")
      for(k2 <- k._2) println(k2)
    }
    

    
    val m = Map[String,String]("Land" -> "USA", "Ort" -> "Gotham City")
    val n = Map[String,String]("Kategorie" -> "Geheimversteck")
    
    println("\nGesuchtes mit m:")
    for(i <- LocationListe.filter(m)) println(i)
    println("\nGesuchtes mit n:")
    for(i <- LocationListe.filter(n)) println(i)
    
    LocationListe.loeschen(batcave)
    println("\nDie Location-Liste nach loeschen:")
    for(i <- LocationListe.locations)  println(i)
    
    println("NIGGA")
    println("hier Beginnt Uebung 6")
    println("\n\n#########################################################################\nUebung 6:")
    
    println("\nBreweries Franken:")
    for(i <- Importer.liesLoc("breweries_franken.csv")) {
      println(i) 
      println(i.adresse)
      println("Aenderungsdatum: " + i.aenderungsdatum)
      println("Kategorien: " + i.kategorien)
      println("Bewertung: " + "*"*i.bewertung)
      println("Bemerkungen: "+ i.bemerkung)
      println
    }
    
    println("\nCategories:")
    for(i <- Importer.liesKat("categories.csv")){
      print(i + " ")
      println(i.icon)
    }
    
    val locs = ImporterJSON.liesLocations("breweries_franken_categories.geojson.txt")
    val kats = ImporterJSON.liesKategorien("breweries_franken_categories.geojson.txt")
    
    println("\nJSON-Mist:")
    println("\nLocations")
    for(i <- locs) println(i)
    println("\nKategorien:")
    for(i <- kats) println(i)
    
    ExporterJSON.export(locs, kats, "blitzkriegJSON.geojson")
  }
  
  
  
}