import org.json.simple.JSONObject
import org.json.simple.JSONArray
import org.json.simple.parser.JSONParser

object ImporterJSON {
  def liesLocations(dateiname:String):List[Location] = {
    var locations:List[Location] = Nil
    val input = scala.io.Source.fromFile(dateiname).mkString
    
    var parser = new JSONParser
    var root = parser.parse(input).asInstanceOf[JSONObject].get("features").asInstanceOf[JSONArray]
      for(i <- 0 to root.size() - 1) {
        var entry = root.get(i).asInstanceOf[JSONObject]
        locations ::= liesLocation(entry)
      }
    locations
  }
  
  def liesLocation(entry:JSONObject):Location = {
    val id = entry.get("id").asInstanceOf[Long].toInt.toString
    val name = entry.get("properties").asInstanceOf[JSONObject].get("name").asInstanceOf[String]
    val bemerkung = entry.get("properties").asInstanceOf[JSONObject].get("note").asInstanceOf[String]
    val strasse = entry.get("properties").asInstanceOf[JSONObject].get("address").asInstanceOf[String]
    val plz = entry.get("properties").asInstanceOf[JSONObject].get("zipcode").asInstanceOf[Long].toInt
    val ort = entry.get("properties").asInstanceOf[JSONObject].get("village").asInstanceOf[String]
    val land = entry.get("properties").asInstanceOf[JSONObject].get("state").asInstanceOf[String]
    val website = entry.get("properties").asInstanceOf[JSONObject].get("web").asInstanceOf[String]
    val bewertung = entry.get("properties").asInstanceOf[JSONObject].get("rating").asInstanceOf[Long].toInt
    val telefon = entry.get("properties").asInstanceOf[JSONObject].get("phone").asInstanceOf[String]
    val email = entry.get("properties").asInstanceOf[JSONObject].get("email").asInstanceOf[String]
    val crDate = entry.get("properties").asInstanceOf[JSONObject].get("creationDate").asInstanceOf[String]
    val edDate = entry.get("properties").asInstanceOf[JSONObject].get("editDate").asInstanceOf[String]
    val besitzer = entry.get("properties").asInstanceOf[JSONObject].get("owner").asInstanceOf[String]
    val kategorien = entry.get("properties").asInstanceOf[JSONObject].get("categories").asInstanceOf[JSONArray]
    val breite = entry.get("geometry").asInstanceOf[JSONObject].get("coordinates").asInstanceOf[JSONArray].get(0).asInstanceOf[Double].toString
    val laenge = entry.get("geometry").asInstanceOf[JSONObject].get("coordinates").asInstanceOf[JSONArray].get(1).asInstanceOf[Double].toString
    
    val adresse = Adresse(strasse, plz, ort, land)
    adresse.website = website
    adresse.telefon = telefon
    adresse.email = email
    val benutzer = Benutzer(besitzer,(math.random*10000).toInt.toString)
    val location = Location(name, id, crDate, benutzer, (breite,laenge))
    location.adresse = adresse
    location.bemerkung ::= bemerkung
    location.bewertung = bewertung
    var kategorieList:List[Kategorie] = Nil
    for(i <- 0 to kategorien.size - 1) {
      val kat = kategorien.get(i).asInstanceOf[String]
      kategorieList ::= Kategorie(kat)
    }
    location.kategorien = kategorieList
    
    location
  }
  
  def liesKategorien(dateiname:String):List[Kategorie] = {
      var kategorien:List[Kategorie] = Nil
      var input = scala.io.Source.fromFile(dateiname).mkString
  
      var parser = new JSONParser
      var root = parser.parse(input).asInstanceOf[JSONObject].get("categories").asInstanceOf[JSONArray]
      for (i <- 0 to root.size - 1) {
        var entry = root.get(i).asInstanceOf[JSONObject]
        kategorien ::= liesKategorie(entry)
      }
      kategorien
  }
  
  def liesKategorie(entry:JSONObject):Kategorie = {
    val name = entry.get("name").asInstanceOf[String]
    val icon = entry.get("icon").asInstanceOf[String]
    
    val foto = Foto(icon)
    val kategorie = Kategorie(name)
    kategorie.icon = foto
    
    kategorie
  }
}