package auffuehrungssystem.test

import auffuehrungssystem._
import org.junit.Test
import org.junit.Assert._

class VorstellungTest {
  @Test def testBelegeSitze = {
    val stueck = new Stueck ("Die Hard", "langsames Sterben", Stuecktyp.FILM, java.util.Date.from(java.time.Instant.now)) 
    val adresse = new Adresse ("Strasse 1", "Ort", 1)
    val ort = new Auffuehrungsort ("Arschtritthalle", adresse) 
    val vorstellung = new Vorstellung(stueck, ort, "7 Uhr eins")

    assertTrue(vorstellung.belegeSitze(100))
    assertTrue(!vorstellung.belegeSitze(101))
    assertEquals(vorstellung.preis*vorstellung.sitzeBelegt, vorstellung.liefereEinnahmen, 0.00001)
    assertTrue(vorstellung.setSitzeGesamt(100))
    assertTrue(!vorstellung.setSitzeGesamt(99))
  }
}